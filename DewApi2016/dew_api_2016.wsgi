#!/usr/bin/python

import sys
import logging
import os

sys.path.insert(0,"/var/www/dew_api_2016/DewApi2016")



activate_this = '/var/www/dew_api_2016/DewApi2016/DewApi2016/venv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

from DewApi2016 import app as application

application.secret_key = 'Add your secret key'
logging.basicConfig(stream=sys.stderr)
